from redbot.core.bot import Red

from .rolex import ROLex


def setup(bot: Red) -> None:
    cog = ROLex(bot)
    bot.add_cog(cog)
    cog.create_task()

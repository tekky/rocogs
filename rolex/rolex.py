import asyncio
import logging
from datetime import datetime

import aiohttp
import discord
from redbot.core import commands
from redbot.core.bot import Red

log = logging.getLogger("red.rocogs.rolex")


class ROLex(commands.Cog):
    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self._task = None

    def cog_unload(self):
        if self._task:
            self._task.cancel()

    def create_task(self):
        def callback(task):
            exc = task.exception()
            if exc is not None:
                log.error("rror", exc_info=exc)

        self._task = asyncio.create_task(self.task())
        self._task.add_done_callback(callback)

    async def task(self):
        await self.bot.wait_until_ready()
        while True:
            now = datetime.utcnow()
            if now.second == 0:
                try:
                    await self.bot.change_presence(
                        activity=discord.Activity(
                            type=discord.ActivityType.watching,
                            name=now.strftime("%H:%M UTC"),
                        )
                    )
                except aiohttp.ClientConnectionError:
                    pass
            await asyncio.sleep(1)
